
Crimson Maidens Mod for Annex: Conquer the World

****************
*Installation*
****************
Unzip archive into the "techs" folder in your application data path.
for example on windows: C:/yourusername/AppData/Roaming/Annex_Ctw/techs/


**********
*About*
**********
With an increasing global conflict over crystal energy, leads to unrest and civil
war in the areas of the world not governed by the three main powers (Alliance,
Republic, and Shadow) of the world. Rebels, Militias, and Gorilla factions,
secretly backed by the great powers, are growing world wide. No where is this
more prevalent than in the Southern Wastelands. Dozens of tribes in the area
waged savage war over territory disputes. In the chaos rose the Crimson Maidens,
whofight to end the conflict in the Southen Wastelands. The Maidens are a female
only army consisting of many victiums of the war, from those who lost their
family, to angry rebels, all the way to elite military defectors who are tired
of fighting for someone else's gain. For years the maidens used bribes and secret
support from all world powers to amass a military large enough, and advanced
enough to dissuade any military action in the region. Their ultimate goal is
to end the world powers influence in the region using the very equipment that
they sold to them.

Crimson Maidens is a remix of Annex units, with some old unused units to make
a new faction.
The style is more of mecha influenced units, with of course female infantry.
This faction is focused on infantry supported by long range combat vehicles.
They lack in any heavily armored vehicles which can make prolonged battles
difficult, however they do have numerous upgrades for their infantry, and
vehicles. Currently they have a hero unit, Falcon but currently no super weapons.


**********************
*Credits/License.*
**********************

Data files are licensed mainly under CC-by-Sa (see "cc-by-sa-3.0-unported.txt"
in the "docs"), for more details read the following text.

The human-readable summary of cc-by-sa-3.0:
Under the following terms: Attribution - You must give appropriate credit,
provide a link to the license, and indicate if changes were made. You may do
so in any reasonable manner, but not in any way that suggests the licensor
endorses you or your use; ShareAlike - If you remix, transform, or build
upon the material, you must distribute your contributions under the same
license as the original.
You are free to: Share - copy and redistribute the material in any medium or
format; Adapt - remix, transform, and build upon the material for any purpose,
even commercially.


******
*Art:*
******

Sand, Road, and Metal floor tiles are made by Adrian Delpha,
under CC-by-Sa license.

Dirt and Grass floor tiles derived from textures made by Tucho Fernandez,
under CC-by-Sa license.

All other art assets including menu, characters, viehicles, buildings, trees,
mountains, and grass in "Annex: Conquer the World" are © 2011 Adrian Delpha,
under the terms of the Creative Commons Attribution-ShareAlike (CC-by-Sa)
3.0 license, or any later version. In brief, this means you are free to share
this work and modify it under the conditions that you provide proper credit to
the author (see instructions below) and license any derivative works under the
same or similar license. To view the full text of the license, visit
"http://creativecommons.org/licenses/by-sa/3.0/" or send a letter to Creative
Commons, 171 Second Street, Suite 300, San Francisco, California, 94105 USA.

You are free to use models as long as proper credit is given, and you abide by
the rules of CC-by-Sa (refer to above), and please Include a link to Annex
(http://www.moddb.com/mods/annex-conquer-the-world) or my portfolio
(http://delphadesign.daportfolio.com/)


********
*Music:*
********

Music obtained through Jamendo http://www.jamendo.com and are under
CC-by-Sa license

In game:
"Prologue" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076983/prologue

"No Return" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076989/no-return

"Overdrive" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076985/overdrive

"The Clash" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1076988/the-clash

"Deathmatch" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1077002/deathmatch

"Black" by Vlad Zhuravlyov  (CC by Sa)
https://www.jamendo.com/en/track/1077001/black


********
*Sounds*
********

Various Guns, Cannons, Rockets, and Explosions Provided by Michael Bethencourt.
(CC by Sa)


********
*Voice*
********

Male Voices: by Levi18

http://freesound.org/people/Levi18/sounds/136396/  Under CC0 License

Female Voices: by Charlotte Duckeet  Under CC by SA License

Female Voice Recordings Copyright 2012 Charlotte Duckett
<https://catalog.librivox.org/people_public.php?peopleid=7315>
Editing Copyright 2012 Iwan Gabovitch <http://qubodup.net>
(Applies to only part of the files, see readme.txt files in archive.)
