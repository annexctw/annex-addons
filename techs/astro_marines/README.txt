﻿Credits for Astro Marines Tech Tree, For Annex: Conquer the World Only

*************
New Factions:
*************

Astro Marines
  Art + Concept by Mr War
  Music Credits:
    Space Fighter Loop by Kevin MacLeod
    http://incompetech.com/m/c/royalty-free/index.html ISRC: US-UAN-11-00672


***************
Annex factions:
***************

Alliance
  Art + Concept by Adrian Delpha
  Music Credits:
   "Industrial Factory" by: Shadrock
   "Astral Assualt" by: Shadrock
   "Epic With a Vengeance" by: Shadrock
   "Under the Influence of Jazz" by: Shadrock
   "Artificial Mind Games" by: Shadrock

Shadow
  Art + Concept by Adrian Delpha
  Music Credits:
   "Contact" by KKSlider60
   "Dependency 2010" by Xenogenocide
   "Sacrifice" by Deathkllr84
